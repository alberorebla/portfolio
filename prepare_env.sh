#!/bin/sh

export JEKYLL_VERSION=3.8
export SMRT_VERSION=2.0.10
export
mkdir build/
wget -O build/smrt.tar.gz https://github.com/sproogen/modern-resume-theme/archive/v${SMRT_VERSION}.tar.gz
tar zxvf build/smrt.tar.gz -C build/ --strip-components 1
cp _config.yml build/
cp -r images build/

#podman run --rm  --volume="$PWD/build:/srv/jekyll"  -p 4000:4000 -it jekyll/jekyll:$JEKYLL_VERSION  jekyll serve
#docker run --rm  --volume="$PWD:/build/srv/jekyll"  -p 4000:4000 -it jekyll/jekyll:$JEKYLL_VERSION  jekyll build
