# This is my portfolio

>There are many like it, but this one is mine.

If you want to see the result you are welcome to see it [here](https://alberorebla.gitlab.io/portfolio/)

---
## What is it?

After spending months searching for some maintainable way to create my CV I discovered this beauty the [modern-resume-theme](https://sproogen.github.io/modern-resume-theme/).
Everything is based on the work that [sproogen](https://github.com/sproogen) is doing. It is amazing please take a look at his projects.

---
# How does it work?

There is no black magic here, the CI downloads the version ```1.8.6``` of the source code of the [modern-resume-theme](https://sproogen.github.io/modern-resume-theme/), it overrides part of the source with the one in this project and does the build using docker.
The second part of the CI is just GitLab pages. Whenever a new push is done in master the CI publish the site on the ```/portfolio``` path.

# How can I create it too?

The author already created the guide to publish on GitHub and this is more or less the full automation for GitLab.
If you want to have your portfolio feel free to fork and create your version.